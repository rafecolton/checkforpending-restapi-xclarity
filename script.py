#!/usr/bin/env python

import http.client
import mimetypes
import ssl
import json
from jsondiff import diff

import config #server ip and password in here

conn = http.client.HTTPSConnection(config.ip, 443, context = ssl._create_unverified_context())
payload = ''
headers = {
  'Authorization': config.pwd
}
conn.request("GET", "/redfish/v1/Systems/1/Bios", payload, headers)
res = conn.getresponse()
current = json.loads(res.read())

conn.request("GET", "/redfish/v1/Systems/1/Bios/Pending", payload, headers)
res = conn.getresponse()
pending = json.loads(res.read())

print("Current Settings: \n")
print(json.dumps(current['Attributes'], indent=2))

print("Pending Settings: \n")
print(json.dumps(pending['Attributes'], indent=2))

print("Changes: \n")
print(diff(current['Attributes'],pending['Attributes']))
